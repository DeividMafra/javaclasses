import java.util.Scanner;

public class First {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		System.out.println(5+4);
//		System.out.println(5-4);
//		System.out.println(5/4);
//		System.out.println(20/4);
//		System.out.println(21%2);
//		System.out.println(Math.pow(10, 2));
		
		System.out.println(5/4.0);
		
//		when you're dividing 2 integer numbers, first the division is made,
//		after that, the result is  storaged in the variable. That's why 
//		int 7 divided by int 2 is equal 3.0 when the result will be storaged
//		in a decimal result. For example:
		
//		int x= 7;
//		int y=2;
//		double result = x/y;
//		System.out.println(result);
		
		double x= 7;
		int y=2;
		double result = x/y;
		System.out.println(result);
		
		Scanner something = new Scanner(System.in);
		System.out.println("Enter your name: ");
		String name = something.nextLine();
		System.out.println(name);
				


	}

}
